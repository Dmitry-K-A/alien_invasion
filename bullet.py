import pygame
from pygame.sprite import Sprite

class Bullet(Sprite):
    """Управление пулями корабля"""

    def __init__(self, ai):
        """Стрельба открывается с позиции корабля"""
        super().__init__()
        self.screen = ai.screen
        self.settings = ai.settings
        self.color = self.settings.bullet_color
        self.bullets = pygame.sprite.Group()

        # Отрисовывает пулю в исходной позиции, затем корректирует её
        self.rect = pygame.Rect(0, 0, self.settings.bullet_width, self.settings.bullet_height)
        self.rect.midright = ai.ship.rect.midright

        # pygame.Rect - объект для

        # Переменная для хранения дробного значения позиции
        self.x = float(self.rect.x)

    def update_bullet(self):
        """Перемещение пули по экрану"""
        # Обновляем скорость перемещения пули исходя из указанного в настройках
        self.x += self.settings.bullet_speed
        # Обновляет положение квадрата
        self.rect.x = self.x

    def draw_bullet(self):
        """Рисует пулю"""
        pygame.draw.rect(self.screen, self.color, self.rect)

    def fire_bullet(self):

        if len(self.bullets) < self.settings.bullets_allowed:
            new_bullet = Bullet()
            self.bullets.add(new_bullet)

    def _update_bullets(self):

        self.bullets.update()

        for bullet in self.bullets.copy():
            if bullet.rect.left >= self.settings.screen_width:
                self.bullets.remove(bullet)