"""
Модуль хранения функций, обеспечивающих работу игры
"""

import sys

import pygame

from ship import Ship

from settings import Settings


class GameFunctions:

    def __init__(self):
        self.ship = Ship(self)
        self.ai_settings = Settings()
        self.screen = pygame.display.set_mode(
            (self.settings.screen_width, self.settings.screen_height))

    # Отслеживаем события клавиатуры и мыши
    def check_events(ship):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:  # Переместить корабль вправо
                    ship.rect.centerx += 1

    # Переносим код обновления экрана в отдельную функцию
    def update_screen(self.ai_settings, self.screen, self.ship):
        screen.blit(self.bg, (0, 0))
        ship.blitme()
        # Отображение последнего прорисованного экрана
        pygame.display.flip()

    if __name__ == '__main__':
        print('Here')
