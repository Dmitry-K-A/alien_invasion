import sys

import pygame

from settings import Settings
from ship import Ship
from bullet import Bullet
from keys import Keys

class AlienInvasion:

    def __init__(self):
        """Инициализация игры; создание объекта экрана"""
        pygame.init()
        self.settings = Settings()
        self.keys = Keys()
        self.bullet = Bullet()
        self.screen = pygame.display.set_mode((self.settings.screen_width, self.settings.screen_height))
        pygame.display.set_caption("Alien Invasion | 0.01 version")
        self.ship = Ship(self)
        self.bullets = pygame.sprite.Group()

    def run_game(self):
        """Главный цикл игры"""
        while True:
            self._update_screen()
            self._update_bullets()
            self.ship.update()
            self.keys.k_check()

    def _update_bullets(self):

        self.bullets.update()

        for bullet in self.bullets.copy():
            if bullet.rect.left >= self.settings.screen_width:
                self.bullets.remove(bullet)

    def _update_screen(self):

        self.screen.blit(self.settings.bg, (0, 0))
        self.ship.blitme()
        for bullet in self.bullets.sprites():
            bullet.draw_bullet()

        pygame.display.flip()


if __name__ == '__main__':
    ai = AlienInvasion()
    ai.run_game()
