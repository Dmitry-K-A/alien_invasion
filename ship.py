import pygame
from settings import Settings
class Ship:


    def __init__(self, ai):

        """Инициализация корабля и его начальной позиции"""
        self.screen = ai.screen
        self.screen_rect = ai.screen.get_rect()
        self.settings = Settings()


        # Отрисовка корабля
        self.image = pygame.image.load("images/spaceship_gr01.png")
        self.rect = self.image.get_rect()

        # Место появления каждого нового корабля
        self.rect.midleft = self.screen_rect.midleft

        # Переменная для десятичного хранения горизонтального положения
        self.x = float(self.rect.x)
        self.y = float(self.rect.y)

        # Флаги движения
        self.moving_right = False
        self.moving_left = False
        self.moving_up = False
        self.moving_down = False

    def update(self):

        """Обновление позиции корабля исходя из флагов движения"""
        # Обновляем значение х
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.x += self.settings.ship_speed

        if self.moving_left and self.rect.left > 0:
            self.x -= self.settings.ship_speed
        # Обновляем квадрат объекта на основании значения х
        self.rect.x = self.x

        # Обновляем значение y
        if self.moving_up and self.rect.y > 0:
            self.y -= self.settings.ship_speed
        if self.moving_down and self.rect.y < self.settings.screen_height:
            self.y += self.settings.ship_speed

        # Обновляем квадрат объекта на основании значения х
        self.rect.y = self.y

    def blitme(self):
        # Рисует корабль в текущей позиции
        self.screen.blit(self.image, self.rect)
