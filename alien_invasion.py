import sys

import pygame

from settings import Settings
from ship import Ship


class AlienInvasion:
    """Инициализация игры; создание объекта экрана"""

    def __init__(self):
        pygame.init()
        pygame.display.set_caption("Alien Invasion | 0.01 version")
        self.settings = Settings()
        self.screen = pygame.display.set_mode((self.settings.screen_width, self.settings.screen_height))
        self.ship = Ship(self)

    def run_game(self):
        """Главный цикл игры"""
        while True:
            self.check_events()
            self.ship.update()
            self.update_screen()

    def check_events(self):
        """прослушка событий клавиатуры и мыши"""
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                self.check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self.check_keyup_events(event)

    def check_keydown_events(self, event):
        """Нажатие клавши"""
        if event.key == pygame.K_q:
            sys.exit()
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = True
        elif event.key == pygame.K_UP:
            self.ship.moving_up = True
        elif event.key == pygame.K_DOWN:
            self.ship.moving_down = True
        elif event.key == pygame.K_RIGHT:
            self.ship.moving_right = True
        #elif event.key == pygame.K_SPACE:
        #    self.bullet.fire_bullet()

    def check_keyup_events(self, event):
        """"""
        if event.key == pygame.K_LEFT:
            self.ship.moving_left = False
        elif event.key == pygame.K_UP:
            self.ship.moving_up = False
        elif event.key == pygame.K_DOWN:
            self.ship.moving_down = False
        elif event.key == pygame.K_RIGHT:
            self.ship.moving_right = False


    def update_screen(self):
        """Перерисовка и "кувырок" экрана"""
        self.screen.blit(self.settings.bg, (0, 0))
        self.ship.blitme()


        # отображение последнего прорисованного экрана
        pygame.display.flip()


if __name__ == '__main__':
    ai = AlienInvasion()
    ai.run_game()
