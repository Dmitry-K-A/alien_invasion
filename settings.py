"""Хранение всех настроек игры"""
import pygame


class Settings:

    def __init__(self):
        # параметры экрана
        self.screen_width = 1200
        self.screen_height = 706
        self.bg = pygame.image.load("images/parallax-space-backgound.png")

        # параметры корабля
        self.ship_speed = 1.5

        # параметры выстрела
        self.bullet_speed = 3.0
        self.bullet_width = 15
        self.bullet_height = 3
        self.bullet_color = (60, 190, 60)
        self.bullets_allowed = 9
